from tkinter import *
from tkinter.messagebox import *
from tkinter.ttk import Combobox
from universes_dictionnaries import *

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                        FUNCTIONS
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# Testing return of combobox
def generate_callback():
    if (universe_combobox.get() != ''):
        names_to_transform = split_name(name_to_transform.get())
        set_to_upper(names_to_transform)
        indexes_array = get_indexes(universe_combobox.get())
        generated_name.set(transform_names(names_to_transform, indexes_array[0], indexes_array[1]))
    else:
        showwarning('Name-Generator', "Please select an universe !")

# Split name in an array of strings (separated by ' ')
def split_name(given_name):
    name_array = given_name.split(' ')
    return name_array

# Set the two first elements in the array to uppercase
def set_to_upper(given_array):
    if (len(given_array) == 2):
        given_array[0] = given_array[0].upper()
        given_array[1] = given_array[1].upper()
    else:
        showwarning('Name-Generator',"Name is incorrect !")

# Get indexes for the names to select the right char
def get_indexes(univ_name):
    index1 = universe[univ_name]['Firstname']['index']
    index2 = universe[univ_name]['Secondname']['index']
    index_tab = [index1, index2]
    return index_tab

# Traduct the first two elements in the universe (chosen) name
def transform_names(given_array, index_firstname, index_secondname):
    if (len(given_array[0]) >= index_firstname - 1 and len(given_array[1]) >= index_secondname - 1):
        letters_chosen = [given_array[0][index_firstname - 1] ,given_array[1][index_secondname - 1]]
        transformed_name = universe[universe_combobox.get()]['Firstname'][letters_chosen[0]] + ' ' + universe[universe_combobox.get()]['Secondname'][letters_chosen[1]]
        return transformed_name
    else:
        showwarning('Name-Generator', "Unfortunately, your name doesn't match for this universe !")


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
                        MAIN PROGRAM
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# Settings of the main frame
frame = Tk()

# StringVar declarations
name_to_transform = StringVar()
universe_name = StringVar()
generated_name = StringVar()

# Widgets configuration

name_label = Label(frame, text = "Enter your name :")
real_name_entry = Entry(frame, textvariable = name_to_transform, width = 30)
generate_button = Button(frame, text = "Generate name !", command = generate_callback)
new_name_label1 = Label(frame, text = "Your ", pady = 10, padx = 1)
universe_combobox = Combobox(frame, state = "readonly", values = universes, width = 25)
new_name_label2 = Label(frame, text = "name is :",pady = 10, padx = 1)
new_generated_name_label = Label(frame, textvariable = generated_name, width = 50, height = 2)

# Rows configuration
frame.rowconfigure(0, weight = 1)
frame.rowconfigure(1, weight = 1)
frame.rowconfigure(2, weight = 1)
frame.rowconfigure(3, weight = 1)
frame.rowconfigure(4, weight = 1)
frame.rowconfigure(5, weight = 1)
frame.rowconfigure(6, weight = 1)

# Columns configuration
frame.columnconfigure(0, weight = 1)
frame.columnconfigure(1, weight = 1)
frame.columnconfigure(2, weight = 1)

# Placing Widgets
name_label.grid(row = 0, columnspan = 3)
real_name_entry.grid(row = 1, columnspan = 3)
new_name_label1.grid(row = 3, column = 0)
universe_combobox.grid(row = 3, column = 1)
new_name_label2.grid(row = 3, column = 2)
new_generated_name_label.grid(row = 4, columnspan = 3)
generate_button.grid(row = 5,columnspan = 3)

# Forbid resize
frame.resizable(FALSE,FALSE)

frame.mainloop()
