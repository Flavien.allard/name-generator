# List of universes included
universes = ['Star Wars', 'Game Of Thrones sword','Metal Gear']

# Dictionnaries of name (for an universe)
universe = {
    'Star Wars' : {
        'Firstname' :{
            'index' : 1,
            'A' : 'Darth',
            'B' : 'Padmé',
            'C' : 'Jar Jar',
            'D' : 'Obi-wan',
            'E' : 'Emperor',
            'F' : 'Han',
            'G' : 'Greedo',
            'H' : 'Anakin',
            'I' : 'Count',
            'J' : 'Sarlacc',
            'K' : 'Jabba',
            'L' : 'Sergeant',
            'M' : 'Boba',
            'N' : 'Wicket W',
            'O' : 'Wampa',
            'P' : 'Senator',
            'Q' : 'Queen',
            'R' : 'Padawan',
            'S' : 'Imperial',
            'T' : 'General',
            'U' : 'Storm',
            'V' : 'R2',
            'W' : 'Luke',
            'X' : 'C3',
            'Y' : 'Mace',
            'Z' : 'Princess'},
        'Secondname' : {
            'index' : 3,
            'A' : 'Bane',
            'B' : 'Sidious',
            'C' : 'Skywalker',
            'D' : 'Chewbacca',
            'E' : 'Fett',
            'F' : 'Trooper',
            'G' : 'PO',
            'H' : 'Speeder',
            'I' : 'Docku',
            'J' : 'Pyt',
            'K' : 'Vader',
            'L' : 'D2',
            'M' : 'Palpatine',
            'N' : 'Grievous',
            'O' : 'Binks',
            'P' : 'Soldier',
            'Q' : 'Kenobi',
            'R' : 'Solo',
            'S' : 'The Hutt',
            'T' : 'Maul',
            'U' : 'Windu',
            'V' : 'Queen',
            'W' : 'Leus',
            'X' : 'Monster',
            'Y' : 'Yoda',
            'Z' : 'Warrick'
            },
        },

    'Game Of Thrones sword' : {
        'Firstname' : {
            'index' : 1,
            'A' : 'Bloody',
            'B' : 'Vulgaz',
            'C' : 'Maddening',
            'D' : 'Ragged',
            'E' : 'Sordid',
            'F' : 'Mortal',
            'G' : 'Barbarous',
            'H' : 'Subborn',
            'I' : 'Cruel',
            'J' : 'Bitter',
            'K' : 'Jagged',
            'L' : 'Haunted',
            'M' : 'Sovereign',
            'N' : 'Crimson',
            'O' : 'Mighty',
            'P' : 'Ferral',
            'Q' : 'Imperial',
            'R' : 'Jagged',
            'S' : 'Lethal',
            'T' : 'Ferocious',
            'U' : 'Intrepid',
            'V' : 'Somber',
            'W' : 'Maddening',
            'X' : 'Ruthless',
            'Y' : 'Sacred',
            'Z' : 'Unforgiving'},
        'Secondname' : {
            'index' : 1,
            'A' : 'Heartstriker',
            'B' : 'Nightfall',
            'C' : 'Vagabond',
            'D' : 'Oath',
            'E' : 'Judge',
            'F' : 'Breaker',
            'G' : 'Mayhem',
            'H' : 'Spirit',
            'I' : 'Commander',
            'J' : 'Marvel',
            'K' : 'Fire',
            'L' : 'Destroyer',
            'M' : 'Widowmaker',
            'N' : 'Vengance',
            'O' : 'Storm',
            'P' : 'Flame',
            'Q' : 'Blade',
            'R' : 'Chaos',
            'S' : 'Giant',
            'T' : 'Steel',
            'U' : 'Thunder',
            'V' : 'Hellhound',
            'W' : 'Butcher',
            'X' : 'Chaos',
            'Y' : 'Sorrow',
            'Z' : 'Jammer'
            }
        },
    'Metal Gear' : {
        'Firstname' : {
            'index' : 3,
            'A' : 'Solid',
            'B' : 'Big',
            'C' : 'Gray',
            'D' : 'Dr.',
            'E' : 'Kyle',
            'F' : 'Roy',
            'G' : 'Master',
            'H' : 'Kio',
            'I' : 'Holly',
            'J' : 'Gustava',
            'K' : 'Revolver',
            'L' : 'Liquid',
            'M' : 'Naomi',
            'N' : 'Meryl',
            'O' : 'Mei',
            'P' : 'Johnny',
            'Q' : 'Psycho',
            'R' : 'Sniper',
            'S' : 'Vulcan',
            'T' : 'Decoy',
            'U' : 'Natasha',
            'V' : 'Raiden',
            'W' : 'Hideo',
            'X' : 'Solidus',
            'Y' : 'Raging',
            'Z' : 'Crying'},
        'Secondname' : {
            'index' : 2,
            'A' : 'Wolf',
            'B' : 'Raven',
            'C' : 'Octopus',
            'D' : 'Kojima',
            'E' : 'Snake',
            'F' : 'Boss',
            'G' : 'Fox',
            'H' : 'Madnar',
            'I' : 'Schneider',
            'J' : 'Campbell',
            'K' : 'Miller',
            'L' : '',
            'M' : 'Marv',
            'N' : 'Ocelot',
            'O' : 'Hunter',
            'P' : 'Jacobsen',
            'Q' : 'Kasler',
            'R' : 'Silverburgh',
            'S' : 'Ling',
            'T' : 'Sasaki',
            'U' : 'Mantis',
            'V' : 'Romanenko',
            'W' : 'Baker',
            'X' : 'Houseman',
            'Y' : 'Dolph',
            'Z' : 'Gurlukovich'
        }
    }
}
