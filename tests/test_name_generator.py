import unittest
from name_generator import *

class Test_name_generator(unittest.TestCase):
    def test_name_transformation(self):
        self.assertEqual(split_name('Leane Allard'), ['Leane','Allard'])
        self.assertEqual(split_name('Bruce Wayne'), ['Bruce','Wayne'])
        self.assertEqual(split_name('Maurice Du Pont'), ['Maurice','Du','Pont'])
        self.assertEqual(split_name('test;test'), ['test;test'])

    def test_get_indexes(self):
        self.assertEqual(get_indexes('Star Wars'), [1,1])
        self.assertEqual(get_indexes('Game Of Thrones sword'), [1,3])
        with self.assertRaises(KeyError):
            self.assertEqual(get_indexes('test'), [1,5])

